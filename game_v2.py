from random import randint

name = input('Hi! What is your name?: ' ).upper()

for guess_number in range(1, 6):
    month_number = randint(1, 12)
    year_number = randint(1924, 2004)

    print(f'Guess {guess_number} : {name} were you born in {month_number} / {year_number}?\n' )

    response = input('Answer "yes" or "no" in lowercase: ').lower()

    if response == "yes":
        print('I knew it!')
        exit()
    elif guess_number == 5:
        print('\n I have other things to do. Good bye.')
    else:
        print('Drat! Lemme try again')
